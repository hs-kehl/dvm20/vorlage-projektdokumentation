---
title: 'Vorlage Projektdokumentation'
abstract: 'Diese Vorlage kann für die Erarbeitung der Projektdokumentation im Rahmen der Vorlesung "Modul 1.7 – IT-Management" im Wintersemester 2022/2023 an der Hochschule Kehl verwendet werden.'
author:
    - Eduard Itrich
    - John Doe
    - Max Mustermannn
date: \today

link-citations: true
toc: true
toc-title: Inhaltsverzeichnis
nocite: '@*'
titlepage: true
toc-own-page: true
titlepage-logo: "images/logo_HSKE.pdf"
logo-width: 130mm
---

# Einleitung

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus vel nisi mi. Pellentesque ac ipsum mattis, consectetur turpis eu, ullamcorper urna. Ut cursus porta laoreet. Praesent facilisis ante nec sem cursus fringilla. Ut iaculis euismod dolor eu viverra. In molestie sit amet nisl eu gravida. Aenean in viverra nunc.[^FußnoteBeispiel]

# Theoretische Grundlagen

Dies ist ein **Typoblindtext**. An ihm kann man sehen, ob alle Buchstaben da sind und wie sie aussehen. Manchmal benutzt man Worte wie Hamburgefonts, Rafgenduks oder Handgloves, um Schriften zu testen. Manchmal Sätze, die alle Buchstaben des Alphabets enthalten - man nennt diese Sätze *Pangrams*. Sehr bekannt ist dieser:

> *The quick brown fox jumps over the lazy old dog.* – [@HandbuchKrisenresilienz]

# Projektskizze

Ein kompakter aber nahezu vollständiger Spickzettel zu Markdown findet sich [hier](https://www.heise.de/downloads/18/1/1/6/7/1/0/3/Markdown-CheatSheet-Deutsch.pdf).

## Anforderungen

![OpenCoDe](./images/opencode.png)

## Leistungsumfang

# Fazit

Curabitur tempus sit amet sapien non ultrices. Nunc ornare tellus arcu, in pretium neque fringilla at. Nullam hendrerit lorem molestie, volutpat mauris sed, pretium nisi. Praesent imperdiet ornare sollicitudin. Nam sollicitudin feugiat dui ut vestibulum. Sed aliquam sem efficitur libero sagittis, scelerisque feugiat massa tempus. Ut porttitor est ac neque scelerisque, et fringilla arcu consequat. Sed eu libero aliquet, malesuada ante a, aliquet nisi. Maecenas tempor purus pellentesque, maximus nisl ac, consequat arcu. Nulla vel ultricies dolor. Aenean ut pharetra ante, et maximus eros.

Das ist ein Fazit dieser Arbeit.

# Literaturverzeichnis

<!--
## Referenzen
-->
[^FußnoteBeispiel]: Dies ist eine Fußzeile.
